# About the project

Rest API server with NodeJS, Express and MongoDB that allows to analyze and detect mutated DNA sequences. The sequences are stored in a database to later provide statistics.


## Objective

It must receive an array of string as a parameter, that represents each row of a table (NxN) with the DNA sequence. The characters of the string sequence can only be: (A, T, C, G), which represents each nitrogenous base. You must validate that you can only receive valid nitrogenous bases.

A mutation exists if more than one sequence of four equal letters is found, obliquely, horizontally, or vertically.



#### Without mutation:

`A T G C G A` <br/>
`C A G T G C` <br/>
`T T A T T T` <br/>
`A G A C G G` <br/>
`G C G T C A` <br/>
`T C A C T G` <br/>

#### With mutation:

`A T G C G A` <br/>
`C A G T G C` <br/>
`T T A T G T` <br/>
`A G A A G G` <br/>
`C C C C T A` <br/>
`T C A C T G` <br/>

## Usage

### Local

Run in project directory

`npm install`

and

`npm run nodemon`

then go to

http://localhost:3000

### Remote

Go to

https://dna-server-nodejs.herokuapp.com/


## Routes
### `POST`
#### Local
http://localhost:3000/mutation

#### Remoto
https://dna-server-nodejs.herokuapp.com/mutation

#### body

{
    dna: < param >
}

Replace < param > with the DNA sequence that must be a Array of String.
Important the Array and the String must have the same length.

Example:

{ “dna”:["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"] }

### Response

#### `status 200`
If there is a mutation.

#### `status 403`
If there is no mutation.

#### `status 400`
If the DNA is invalid or required

#### `status 500`
If there is an internal server error.

### `GET`
#### Local
http://localhost:3000/stats

#### Remoto
https://dna-server-nodejs.herokuapp.com/stats

### Response

#### `status 200`

for example

{
    "count_mutations": 3,
    "count_no_mutations": 14,
    "ratio": 0.21428571428571427
}

#### `status 500`
If there is an internal server error.


## Test

### Postman

#### Test 1

![Alt text](./assets/images/test1.png?raw=true "Test DNA without mutation")

#### Test 2

![Alt text](./assets/images/test2.png?raw=true "Test DNA with mutation")

#### Test 3

![Alt text](./assets/images/test3.png?raw=true "Test stats")



## Used packages

### express

Fast, unopinionated, minimalist web framework for node.

https://www.npmjs.com/package/express

### mongoose

Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment. Mongoose supports both promises and callbacks.

https://www.npmjs.com/package/mongoose

### mongoose-unique-validator

mongoose-unique-validator is a plugin which adds pre-save validation for unique fields within a Mongoose schema.

https://www.npmjs.com/package/mongoose-unique-validator

### body-parser

Node.js body parsing middleware.

Parse incoming request bodies in a middleware before your handlers, available under the req.body property.

https://www.npmjs.com/package/body-parser

### Nodemon

Nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.

https://www.npmjs.com/package/nodemon

