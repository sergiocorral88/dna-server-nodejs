const DeoxyribonucleicAcid = require('../models/dna.model');
const dnaService = require('../services/dna.services');

/** 
 *  This function returns if a DNA has a mutation and is in charge of storing it in the database. 
 *  body { dna: ['ATGCGA','CAGTGC','TTATGT','AGAAGG','CCCCTA','TCACTG'] }
 *  dna is required.
 *  The matrix must be NxN
 *  The characters of the String sequence can only be: (A, T, C, G).
 *  A mutation exists if more than one sequence of four equal letters is found, obliquely, horizontally, or vertically.
 */
const hasMutation = (req, res) => {

    /** Get the dna or null */
    const dnaParam = req.body.dna || null;

    /** Validates the DNA parameter and returns an error if it does not exists or is invalid*/
    if (!dnaParam) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'DNA is required.'
            }
        });
    } else if (!dnaService.isValidNitrogenBase(dnaParam)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: `The DNA ${dnaParam} is not a valid DNA. Can only contain characters A, T, C, G and the matrix must be NxN. Example body { dna: ['ATGCGA','CAGTGC','TTATGT','AGAAGG','CCCCTA','TCACTG'] }`
            }
        });
    }

    // Get all sequences obliquely, horizontally and vertically.
    const sequences = dnaService.getAllSequences(dnaParam);

    // Analyze each sequence to see if it is mutant. If more than one is mutant it returns true
    const isMutant = sequences.filter(sequence => dnaService.isMutantSequence(sequence)).length > 1;

    // Create a instance of DNA
    let dnaDocument = new DeoxyribonucleicAcid({
        dna: dnaParam,
        mutation: isMutant
    });

    // Save instance of DNA
    dnaDocument.save((err, dnaDB) => {

        // If there is an internal server error.
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        // If there is no mutation return 403.
        if (!dnaDB.mutation)
            return res.status(403).json({
                ok: true,
                message: 'DNA without mutation.'
            });

        // If there is a mutation return 403.
        res.json({
            ok: true,
            message: 'DNA with mutation.'
        });

    });

}



module.exports = {
    hasMutation
};