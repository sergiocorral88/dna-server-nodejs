const deoxyribonucleicAcid = require('../models/dna.model');

/** Returns the statistics of the DNA checks stored in the database.  
 *  { 
 *    count_mutations: number of DNA with mutation
 *    count_no_mutations: number of DNA without mutation
 *    ratio: with mutation / without mutation
 *  }
 */
const getStatsMutationDNA = (req, res) => {

    // Execute the query for statistics
    deoxyribonucleicAcid.aggregate().group({

        _id: { name: "$name" },
        count_mutations: { $sum: { $cond: ["$mutation", 1, 0] } },
        count_no_mutations: { $sum: { $cond: ["$mutation", 0, 1] } }

    }).exec((err, stats) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        let ratio = 0;

        // if there is a result and the denominator is greater than zero, calculate the ratio
        if (stats.length && stats[0].count_no_mutations)
            ratio = stats[0].count_mutations / stats[0].count_no_mutations;

        // Set response
        res.json({
            count_mutations: stats.length ? stats[0].count_mutations : 0,
            count_no_mutations: stats.length ? stats[0].count_no_mutations : 0,
            ratio: ratio
        });

    });

}

module.exports = {
    getStatsMutationDNA
};