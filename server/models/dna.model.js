const mongoose = require('mongoose');
const dnaService = require('../services/dna.services');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

/** Create the DNA schema */
let dnaSchema = new Schema({
    dna: {
        type: Object,
        required: [true, 'DNA sequence is required.'],
        validate: {
            validator: function(v) {
                return dnaService.isValidNitrogenBase(v);
            },
            message: props => `${props.value} is not a valid DNA. Can only contain characters A, T, C, G and the matrix must be NxN.`
        },
    },
    mutation: { type: Boolean, requiered: [true] }
}, {
    timestamps: true
});

/** Change the attribute "_id" for "id" in the JSON */
dnaSchema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

dnaSchema.plugin(uniqueValidator, { message: '{PATH} it must be unique.' });

module.exports = mongoose.model('DeoxyribonucleicAcid', dnaSchema);