module.exports = app => {

    const dnaController = require('../controllers/dna.controller');
    const statisticController = require('../controllers/statistic.controller');

    const router = require("express").Router();

    router.post('/mutation', dnaController.hasMutation);

    router.get('/stats', statisticController.getStatsMutationDNA);

    app.use('', router);
};