/** Analyze each sequence to see if it is mutant. If more than one is mutant it returns true */
const isMutantSequence = (sequence) => {

    return sequence.length > 3 && /A{4}|T{4}|G{4}|C{4}/.test(sequence);

};

/** Returns all oblique, horizontal, and vertical sequences. */
const getAllSequences = (dna) => {

    // Get N of matrix NxN
    const dimension = dna.length;

    const matrix = arrayToMatrix(dna);

    // stores all oblique, horizontal, and vertical sequences.
    let sequences = [];

    // Add all horizontal sequences.
    sequences = sequences.concat(dna);

    for (let column = 0; column < dimension; column++) {

        let verticalSecuenceTemp = '';

        for (let row = 0; row < dimension; row++) {

            // add a vertical sequence character
            verticalSecuenceTemp += matrix[row][column];

            // get the right diagonal
            if (column === 0 || row === 0)
                sequences.push(rightDiagonal(row, column, matrix, dimension));

            // get the left diagonal
            if (row === 0 || column === (dimension - 1))
                sequences.push(leftDiagonal(row, column, matrix, dimension));

        }

        // add vertical sequence
        sequences.push(verticalSecuenceTemp);

    }

    return sequences;

};


/** Returns the right diagonal from a starting element.
 *  Example matrix 4x4 with starting element (0;0) the right diagonal
 *  is (0;0), (1;1), (2;2), (3;3), (4;4)
 *  __ _ _ __
 * | X         <- start
 * |   X 
 * |     X
 * |       X   <- end
 * 
 */
const rightDiagonal = (numberRow, numberColumn, matrix, dimension) => {

    let sequence = '';

    while (numberRow < dimension && numberColumn < dimension) {

        sequence += matrix[numberRow][numberColumn];

        numberRow++;

        numberColumn++;

    }

    return sequence;
};

/** Returns the left diagonal from a starting element.
 *  Example matrix 4x4 with starting element (0;4) the left diagonal 
 *  is (0;4), (1;3), (2;2), (3;1), (4;0)
 * __ _ _ __
 *        X| <- start
 *      X  |
 *    X    |
 *  X      | <- end
 * 
 */
const leftDiagonal = (numberRow, numberColumn, matrix, dimension) => {

    let sequence = '';

    while (numberRow < dimension && 0 <= numberColumn) {

        sequence += matrix[numberRow][numberColumn];

        numberRow++;

        numberColumn--;

    }

    return sequence;

};

/** 
 *  Verify that the DNA is valid.
 *  it must be of dimension "NxN" and the characters 
 *  of the string sequence can only be: (A, T, C, G) 
 */
const isValidNitrogenBase = (dna) => {

    if (!Array.isArray(dna)) {
        return false;
    }

    const dimension = dna.length;

    return dna.every((sequence) => sequence.length === dimension && /^[TACG]+$/.test(sequence));

}

/** Convert a array of string to a matrix of characters */
const arrayToMatrix = (dna) => {

    let matrix = [];

    dna.forEach((sequence, index) => matrix[index] = Array.from(sequence));

    return matrix;

};

module.exports = {
    getAllSequences,
    isMutantSequence,
    isValidNitrogenBase
};